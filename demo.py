import os

import numpy as np
from PIL import Image

import chainer
import chainer.cuda
from chainer import Variable

from net import Encoder
from net import Decoder


def exec_p2p(in_path, out_path):
    enc_path = "model/enc_iter_145000.npz"
    dec_path = "model/dec_iter_145000.npz"

    enc = Encoder(in_ch=1)
    dec = Decoder(out_ch=3)
    chainer.serializers.load_npz(enc_path, enc)
    chainer.serializers.load_npz(dec_path, dec)
    print("loaded")

    w_in = 256
    w_out = 256
    in_ch = 3
    out_ch = 3
    out_image(in_path, out_path, enc, dec)


def out_image(in_path, out_path, enc, dec):
    print("out_image being")
    chainer.config.train=False
    xp = enc.xp

    w_in = 256
    w_out = 256
    in_ch = 1
    out_ch = 3

    #in_all = np.zeros((1, out_ch, w_out, w_out)).astype("f")
    x_in = xp.zeros((1, in_ch, w_in, w_in)).astype("f")
 
    print("image open")
    img = Image.open(in_path)
    img = img.convert('L')
    w,h = img.size
    #r = 256 / float(min(w,h))
    #img = img.resize((int(r*w), int(r*h)), Image.BILINEAR)
    #x_in = np.asarray(img).astype("f").transpose(2,0,1)/128.0-1.0
    img = img.resize((256, 256), Image.NEAREST)
    img = np.asarray(img).astype("f")
    img = img[:,:,np.newaxis]
    x_in[0,:] = img.transpose(2,0,1)/128.0-1.0

    print(x_in.shape)
    x_in = Variable(x_in)
    
    print("enc")
    z = enc(x_in)
    print("dec")
    x_out = dec(z)
    print("out data")
    x_out = x_out.data[0,:]

    def save_image(x):
        print("save_image being")
        x = x.transpose(1,2,0)
        x = np.asarray(np.clip(x * 128 + 128, 0.0, 255.0), dtype=np.uint8)
        print(x)
        import cv2
        x = cv2.resize(x, (w,h))
        cv2.imwrite(out_path, x)

        #Image.fromarray(x).convert('RGB').save("genout.jpg")

    #x = np.asarray(np.clip(gen_all * 128 + 128, 0.0, 255.0), dtype=np.uint8)
    save_image(x_out)

if __name__ == '__main__':
    exec_p2p("test1.png","out.jpg")