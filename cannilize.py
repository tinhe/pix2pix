import os,glob
import cv2
import numpy as np

for img_path in glob.glob("./face/val/a/*.jpg"):
    print(img_path)
    basename = os.path.basename(img_path)
    img = cv2.imread(img_path)
    kernel = np.ones((5,5),np.float32)/25
    img = cv2.filter2D(img,-1,kernel)
    img = cv2.Canny(img, 30, 80)
    img = cv2.bitwise_not(img)
    print("./face/train/c/"+basename[:-3]+"png")
    cv2.imwrite("./face/val/c/"+basename[:-3]+"png", img)
