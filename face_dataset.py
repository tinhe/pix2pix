import random
import matplotlib
matplotlib.use("Agg")
import json
import os
import numpy as np
import cv2

import chainer
from chainer import Variable
from chainer.dataset import dataset_mixin

from chainercv.transforms import flip

def transform_image(img):
    img = img.transpose(1, 2, 0)
    img += 1
    img /= 2
    img *= 255.0
    return img

class FaceDataset(dataset_mixin.DatasetMixin):
    
    def __init__(self, image_root, gt_root, size=(256, 256), random_flip=True, image_color=True,label_color=True):
        self.random_flip = random_flip
        self.size = size
        self.label_color = label_color
        self.image_color = image_color
        self.pairs = self.build_list(image_root, gt_root)

        self.input_channels = 3

    def build_list(self, _root, _gtroot):
        
        import glob
        if self.image_color:
            imgs = glob.glob(os.path.join(_root,"*.jpg"))
        else:
            imgs = glob.glob(os.path.join(_root,"*.png"))
        
        pairs = []
        for img in imgs:
            fname = os.path.basename(img)
            fullpath = img
            if self.label_color:
                gtfullpath = os.path.join(_gtroot,fname)[:-3]+"jpg"
            else:
                gtfullpath = os.path.join(_gtroot,fname)[:-3]+"png"
            pairs.append((fullpath, gtfullpath))

        print("found {} pairs".format(len(pairs)))
        return pairs

    def __len__(self):
        return len(self.pairs)

    def get_image(self, imagepath):
        h, w = self.size
        if self.image_color:
            img = cv2.imread(imagepath, cv2.IMREAD_COLOR).astype(np.float32)
        else:
            img = cv2.imread(imagepath, cv2.IMREAD_GRAYSCALE).astype(np.float32)

        img = cv2.resize(img, (w, h), interpolation=cv2.INTER_LINEAR)
        img /= 255.0
        img *= 2.0
        img -= 1.0
        if not self.image_color:
            img = img[:,:,np.newaxis]
        return img.transpose(2, 0, 1)

    def get_label(self, imagepath):
        h, w = self.size
        if self.label_color:
            img = cv2.imread(imagepath, cv2.IMREAD_COLOR).astype(np.float32)
        else:
            img = cv2.imread(imagepath, cv2.IMREAD_GRAYSCALE).astype(np.float32)

        img = cv2.resize(img, (w, h), interpolation=cv2.INTER_LINEAR)
        
        img /= 255.0
        img *= 2.0
        img -= 1.0
        if not self.label_color:
            img = img[:,:,np.newaxis]
        return img.transpose(2, 0, 1)


    def get_example(self, i):
        imgpath, lblpath = self.pairs[i]

        img = self.get_image(imgpath)
        lbl = self.get_label(lblpath)

        if random.random() < 0.5:
            lbl = flip(lbl, x_flip=True)
            img = flip(img, x_flip=True)

        return lbl, img

    def visualizer(self, output_path="preview", n=1):
        @chainer.training.make_extension()
        def make_image(trainer):
            updater = trainer.updater
            output = os.path.join(trainer.out, output_path)
            os.makedirs(output, exist_ok=True)

            rows = []
            for i in range(n):
                label, image = updater.converter(updater.get_iterator("test").next(), updater.device)

                # turn off train mode
                with chainer.using_config('train', False):
                    generated = updater.get_optimizer("generator").target(label).data

                # convert to cv2 image
                img = transform_image(generated[0])
                label = label[0].transpose(1, 2, 0) #transform_image(label[0])
                image = transform_image(image[0])

                # return image from device if necessary
                if updater.device >= 0:
                    img = img.get()
                    label = label.get()
                    image = image.get()

                rows.append(np.hstack((img, image)).astype(np.uint8))

                cv2.imwrite(os.path.join(output, "iter_{}.png".format(updater.iteration)), np.vstack(rows))

        return make_image
